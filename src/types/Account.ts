export default interface Account {
  application: string;
  emails: string[];
  name: string;
}
