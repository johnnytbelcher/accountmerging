import Account from "./Account";

class PersonClass {
  applications: Set<string>;
  emails: Set<string>;
  name: string;

  constructor(name: string, application: string, emails: string[]) {
    this.name = name;
    this.applications = new Set([application]);
    this.emails = new Set(emails);
  }

  mergePerson(person: PersonClass): void {
    person.applications.forEach((app) => this.applications.add(app));
    person.emails.forEach((email) => this.emails.add(email));
  }

  mergeAccount(account: Account): void {
    this.applications.add(account.application);
    account.emails.forEach((email) => {
      this.emails.add(email);
    });
  }

  getPerson(): Person {
    return {
      name: this.name,
      applications: Array.from(this.applications),
      emails: Array.from(this.emails),
    };
  }
}

export interface Person {
  applications: string[];
  emails: string[];
  name: string;
}

export default PersonClass;
