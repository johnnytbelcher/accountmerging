import { readAccountsFile } from "./FileReader";
import { PersonBuilder } from "./PersonBuilder";
import Account from "./types/Account";

const initialize = async (): Promise<Account[] | undefined> => {
  if (process.argv.length < 3) {
    console.log("No file provided");
  }

  return await readAccountsFile(process.argv[2]);
};

initialize()
  .then((accounts) => {
    if (!accounts) {
      console.log("Unable to read in accounts");
      return;
    }

    const personCreator = new PersonBuilder(accounts);
    personCreator.createPersons();

    const persons = personCreator.getPersons();

    console.log(persons);
    console.log("DONE");
  })
  .catch((e) => {
    console.log("An error occured:");
    console.log(e);
  });
