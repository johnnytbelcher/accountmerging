import Account from "./types/Account";

export const readAccountsFile = async (
  filename: string
): Promise<Account[]> => {
  const accountFile = require(filename);
  const accounts = accountFile as Account[];

  return accounts;
};
