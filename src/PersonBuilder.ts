import Account from "./types/Account";
import PersonClass, { Person } from "./types/Person";

export class PersonBuilder {
  accounts: Account[];
  persons: PersonClass[];

  constructor(accounts: Account[]) {
    this.accounts = accounts;
    this.persons = [];
  }

  createPersons() {
    // keep track of the emails we've seen, and where they are in our person list
    const emailToPerson = new Map<string, number>();
    const persons: PersonClass[] = [];

    // returns an ordered list of all indexes with relationships to the provided emails
    const getPersonIndexes = (emails: string[]): number[] => {
      const indexes: number[] = [];
      for (let i = 0; i < emails.length; i++) {
        const email = emails[i];
        if (emailToPerson.has(email)) {
          indexes.push(emailToPerson.get(email) as number);
        }
      }
      return indexes.sort((a, b) => a - b);
    };

    // turn all Account into PersonClass
    this.accounts.forEach((account) => {
      const personIndexes = getPersonIndexes(account.emails);

      // one other person exists, merge the account into it
      if (personIndexes.length === 1) {
        const mainIndex = personIndexes[0];
        const person = persons[mainIndex];

        person.mergeAccount(account);

        // add each email to the lookup
        account.emails.forEach((email) => {
          emailToPerson.set(email, mainIndex);
        });
      }

      // there was more than one hit, merge everything
      else if (personIndexes.length > 1) {
        const mainIndex = personIndexes[0];
        const person = persons[mainIndex];

        // iterate in reverse order to remove higher indexes first
        for (let i = personIndexes.length - 1; i > 0; i--) {
          const curIndex = personIndexes[i];
          const personToMerge = persons[curIndex];

          person.mergePerson(personToMerge);

          // make sure all references now link back to the correct email
          personToMerge.emails.forEach((email) => {
            emailToPerson.set(email, mainIndex);
          });

          // remove this occurance from the existing array
          persons.splice(curIndex, 1);
        }

        person.mergeAccount(account);

        // add each email to the lookup
        account.emails.forEach((email) => {
          emailToPerson.set(email, mainIndex);
        });
      }

      // the person doesn't exist yet, create it
      else {
        persons.push(
          new PersonClass(account.name, account.application, account.emails)
        );
        account.emails.forEach((email) => {
          const index = persons.length - 1;
          emailToPerson.set(email, index);
        });
      }
    });

    this.persons = persons;
  }

  getPersons(): Person[] {
    return this.persons.map((p) => p.getPerson());
  }
}
